package com.example.messagingstompwebsocket;

import lombok.Data;

@Data
public class HelloMessage {
	private String name;
}
