package payroll;

import lombok.Data;
import lombok.NoArgsConstructor;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.Table;

@Entity
@Data
@NoArgsConstructor
@Table(name = "CUSTOMER_ORDER")
class Order {
	@Id
	@GeneratedValue
	private Long id;

	private String description;

	private Status status;

	Order(String description, Status status) {
		this.description = description;
		this.status = status;
	}
}
