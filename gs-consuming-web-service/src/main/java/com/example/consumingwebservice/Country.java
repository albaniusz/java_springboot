package com.example.consumingwebservice;

import lombok.Data;

@Data
public class Country {
	private String currency;
}
