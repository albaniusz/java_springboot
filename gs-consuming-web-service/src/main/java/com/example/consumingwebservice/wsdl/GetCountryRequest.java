package com.example.consumingwebservice.wsdl;

import lombok.Data;

@Data
public class GetCountryRequest {
	private String name;
}
