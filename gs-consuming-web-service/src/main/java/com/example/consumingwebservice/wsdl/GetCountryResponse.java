package com.example.consumingwebservice.wsdl;

import com.example.consumingwebservice.Country;
import lombok.Data;

@Data
public class GetCountryResponse {
	private Country country;
}
