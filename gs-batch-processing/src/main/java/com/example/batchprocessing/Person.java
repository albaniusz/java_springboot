package com.example.batchprocessing;

import lombok.Data;

@Data
public class Person {
	private String lastName;
	private String firstName;

	public Person(String firstName, String lastName) {
		this.firstName = firstName;
		this.lastName = lastName;
	}
}
